import React from "react";
import { connect } from "react-redux";

export const Albums = ({ props }) => (
  <div>
    <p><strong>Album:</strong> {props.album}</p>
    <pre>{props.song}</pre>
  </div>
);

const mapStateToProps = (state, ownProps) => {
  let album = ownProps.album;
  return {
    album: state.lines.filter((line) => line.album === album)
  };
};
// const mapStateToProps = ({ state }) => {
//   state;
// };

export const ConnectedAlbums = connect(mapStateToProps)(Albums);
