import React from "react";
import { connect } from "react-redux";
import { ConnectedAlbums } from "./Albums";

function getAlbums(lines) {
  let albumArray = [];
  for (let line of lines) {
    albumArray.push(line.album);
  }
  return new Set([...albumArray]);
}

export const Dashboard = ({ lines }) => (
  <div>
    <h2>Dashboard</h2>
    {/* {lines.map((line) => (
      <div key={line.id}>
        <ConnectedAlbums props={line} />
      </div>
    ))} */}
    {console.log(getAlbums(lines))}
  </div>
);

function mapStateToProps(state) {
  return {
    lines: state.lines
  };
}

export const ConnectedDashboard = connect(mapStateToProps)(Dashboard);
